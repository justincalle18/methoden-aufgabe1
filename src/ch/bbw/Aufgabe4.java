package ch.bbw;

public class Aufgabe4 {
    //endsWith, startsWith, equalsIgnoreCase
    public String booleans(String word){
        System.out.println(word);
        System.out.println("begin: " + word.startsWith("Hello"));
        System.out.println("end: " + word.endsWith("world"));

        return word;
    }
}
