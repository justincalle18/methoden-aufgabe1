package ch.bbw;

import java.util.Locale;
import java.util.Scanner;

public class Aufgabe2 {
    Scanner input = new Scanner(System.in);

    public String zusammenfügen(){
        System.out.print("Bitte geben Sie einen String ein: ");
        String text = input.nextLine();
        System.out.print("Bitte geben Sie einen zweiten String ein: ");
        String text2 = input.nextLine();
        String fulltext = text + " " + text2;
        System.out.println(fulltext);
        System.out.println(fulltext.toLowerCase(Locale.ROOT));
        System.out.print("Bitte geben Sie einen Namen ein:");
        String name = input.nextLine();
        System.out.print("Bitte geben Sie eine Domain ein:");
        String domain = input.nextLine();
        String email = name + "@" + domain;
        System.out.println(email);
        return zusammenfügen();

    }
}
