package ch.bbw;

import java.util.Scanner;

public class Aufgabe6 {
    public String NameOhneEndung(){
        Scanner input = new Scanner(System.in);
        System.out.println("Geben Sie Ihren Namen ein:");
        String name = input.nextLine();
        System.out.println(name.substring(0,name.length()-1));
        return NameOhneEndung();
    }
}
